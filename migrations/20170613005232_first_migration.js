
exports.up = function(knex, Promise) {
	return knex.schema.createTable('games',(table)=>{
		table.increments('id').primary().unique();
		table.string('name');
		table.string('added_by');
		table.string('game');
		table.string('url');
		table.string('time');
		table.timestamps(true,true);
	});
};

exports.down = function(knex, Promise) {

};
