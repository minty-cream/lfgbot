const _NODE_ENV=process.env.NODE_ENV;
const server_url='change_me';
const knex=require('knex')(require('./knexfile')[_NODE_ENV]);
const schedule=require('node-schedule');
const irc = require('irc');
knex.migrate.latest()
	.then(()=>{
		const client = new irc.Client('irc.thisisnotatrueending.com','GameBot',{
			channels:['#suptg']
		});

		let _howToUse=()=>{client.say("#suptg","!addgame <game_name>;<game_rulebook>;<game_time>;<pastebin to game description> # The semi-colons are vital | !lfg # List a random game.")};
		let _announceGame=(_comment)=>{
			knex('games')
				.orderByRaw(knex.raw('RANDOM()'))
				.limit(1)
				.then((resp)=>{
					client.say("#suptg",`${_comment} ${resp[0].name} (${resp[0].game}) @ ${resp[0].time} - added by ${resp[0].added_by} - ${resp[0].url} - See more at ${server_url}`);
				})
				.error((error)=>{
					client.say("#suptg",`Something went wrong. ${error}`);
				})
		}

		client.addListener('message',(from,to,message)=>{
			let _message=message.split(/ (.+)/);
			if(_message[0]){
				if(_message[0]==='!addgame'){
					if(_message[1]){
						let _opts=_message[1].split(';');
						if(_opts && _opts.length === 4){
							knex('games')
								.insert({
									name:_opts[0],
									game:_opts[1],
									time:_opts[2],
									url:_opts[3],
									added_by:from
								})
								.then((resp)=>{
									_announceGame('Game succesfully added.');
								})
								.catch((error)=>{
									client.say("#suptg",`Something went wrong. ${error}`);
								});
						}else{
							_howToUse();
						}
					}
				}
				if(_message[0]==='!lfg'){
					_announceGame();
				}
				if(_message[0]==='!help'){
					_howToUse();
				}
			}
		});

		schedule.scheduleJob('42 * * * *', function(){
			_announceGame();
		});
	});
