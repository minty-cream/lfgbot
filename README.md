# README - LFGBot #

This is a fairly simple bot that offers two commands: `!addgame` and `!help` . Its purpose is to announce a random game in its listing every hour, and to optionally display its stored games inside a web site.

Currently, it is only possible to add a game via the IRC bot's `!addgame`command.

It can work with any SQL database supported by knex, but currently is configured to use an SQLite database.

## !help ##

Announces the following to the room:

>!addgame <game_name>;<game_rulebook>;<game_time>;<pastebin to game description> | The semi-colons are vital

## !addgame ##

This command adds a listing to the SQL database of games that are announced hourly. After adding a new game, it announces a random game.

### Parameters ###

####game_name####

The title of the game.

####game_rulebook####

The title of the game's rulebook.

####game_time####

The scheduled time for the game.

####pastebin to game description####

A pastebin link with the full details of the game.