const _NODE_ENV=process.env.NODE_ENV;
const knex=require('knex')(require('./knexfile')[_NODE_ENV]);
const express=require('express');
const app=express();


app.get('/',(req,res)=>{
	knex('games').select()
		.orderByRaw(knex.raw('RANDOM()'))
		.then((resp)=>{
			console.log("resp",resp);
			let _list='';
			_list=resp.reduce((acc,curr)=>{
				return `${acc}<li>${curr.name} (${curr.game}) @ ${curr.time} - added by ${curr.added_by} - ${curr.url}</li>`;
			},_list);
			let _out=`
	<html>
	<head>
	<title>Game Bot</title>
	</head>
	<body>
		<h1>Heres the lists I have stored</h1>
		<ul>
		${_list}
		</ul>
	</body>
	</html>
		`;
			res.send(_out);
		});
});

knex.migrate.latest()
	.then(()=>{
		app.listen(process.env.PORT);
		console.log("Started");
	});
